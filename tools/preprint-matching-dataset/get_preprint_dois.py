import csv
import json
import requests


with open("data/preprint_dois.csv", "w") as f:
    writer = csv.writer(f)
    cursor = "*"
    i = 0
    j = 0
    while True:
        print(i, j)
        i += 1
        params = {
            "cursor": cursor,
            "filter": "until-created-date:2023-08-31",
            "rows": 1000,
        }
        data = requests.get(
            "https://api.crossref.org/types/posted-content/works", params
        ).json()["message"]
        cursor = data["next-cursor"]
        if not data["items"]:
            break
        for work in data["items"]:
            if work.get("subtype", "") != "preprint":
                continue
            work_metadata = {}
            for field in ["title", "issued", "author"]:
                if field in work:
                    work_metadata[field] = work[field]
            writer.writerow([work["DOI"], json.dumps(work_metadata)])
            j += 1
