import argparse
import logging
import matching.openurl_query_matcher
import matching.stq_matcher
import re
import sys
import utils.data_format_keys as dfk

from multiprocessing import Pool
from utils.utils import init_logging, read_json, save_json
from utils.cr_utils import get_item


def find_reference(ref, item):
    key = ref.get('key', 'key-ref')
    for item_ref in item.get('reference', []):
        if key == item_ref.get('key', ''):
            return item_ref
    try:
        num = int(re.sub('.*[^\d](\d*)', '\\1', key))
        for item_ref in item.get('reference', []):
            try:
                item_num = int(re.sub('.*[^\d](\d*)', '\\1',
                                      item_ref.get('key', '')))
                if num == item_num:
                    return item_ref
            except ValueError:
                pass
    except ValueError:
        return None


def extract_references(sample_refs):
    with Pool(20) as p:
        results = p.map(get_item, [s['source_DOI'] for s in sample_refs])
    references = []
    for r, i in zip(sample_refs, results):
        found_ref = find_reference(r, i)
        ref = {'source_DOI': r['source_DOI']}
        ref['reference_previous'] = dict(r)
        del ref['reference_previous']['source_DOI']
        del ref['reference_previous']['DOI_from_structured']
        del ref['reference_previous']['DOI_from_unstructured']
        if found_ref is None:
            ref['reference'] = {}
            ref['DOI_from_OpenURL'] = None
            ref['DOI_from_STQ'] = None
        else:
            if r.get('key', '') == found_ref.get('key', ''):
                del ref['reference_previous']
            ref['reference'] = found_ref
            ref['DOI_from_OpenURL'] = r['DOI_from_structured']
            ref['DOI_from_STQ'] = r['DOI_from_unstructured']
        references.append(ref)

    return references


def match_openurl(references):
    matcher = matching.openurl_query_matcher.Matcher()
    input_refs = [r for r in references
                  if dfk.CR_ITEM_DOI not in r['reference']]
    logging.info('Input references: {}'.format(len(input_refs)))
    with Pool(5) as p:
        results = p.map(matcher.match, [r['reference'] for r in input_refs])
    [item.update({'DOI_from_OpenURL': None}) for item in references]
    for item, doi in zip(input_refs, results):
        item['DOI_from_OpenURL'] = doi[0]


if __name__ == '__main__':
    sys.setrecursionlimit(10000)

    parser = argparse.ArgumentParser(
        description='re-match references from a sample')
    parser.add_argument('-v', '--verbose', help='verbose output',
                        action='store_true')
    parser.add_argument('-s', '--sample', type=str, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)

    args = parser.parse_args()

    init_logging(args.verbose)

    sample_data = read_json(args.sample)

    references = extract_references(sample_data)
    logging.info('Total number of references: {}'.format(len(references)))

    logging.info('Matching with OpenURL')
    match_openurl(references)

    save_json(references, args.output)
