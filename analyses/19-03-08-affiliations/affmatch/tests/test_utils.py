from affmatch.utils import normalize


def test_normalize():
    assert normalize('university of excellence') == 'university of excellence'
    assert normalize('ünivërsity óf éxcellençe') == 'university of excellence'
    assert normalize('University  of    ExceLLence') == \
        'university of excellence'
    assert normalize('The University of Excellence') == \
        'university of excellence'
    assert normalize('University of Excellence & Brilliance') == \
        'university of excellence and brilliance'
    assert normalize('The University of Excellence & Brilliance') == \
        'university of excellence and brilliance'
    assert normalize('U.S. University of Excellence') == \
        'united states university of excellence'
    assert normalize('university of tech') == 'university of technology'
    assert normalize('university of tech & Excellence') == \
        'university of technology and excellence'
    assert normalize('University of Tech. & Excellence') == \
        'university of technology and excellence'
    assert normalize('Inst. of excellence') == 'institute of excellence'
    assert normalize('Inst of Excellence') == 'institute of excellence'
    assert normalize('Inst of Excellence inst') == \
        'institute of excellence institute'
    assert normalize('Lab. of excellence') == 'laboratory of excellence'
    assert normalize('Lab of Excellence') == 'laboratory of excellence'
    assert normalize('lab of Excellence lab') == \
        'laboratory of excellence laboratory'
    assert normalize('Univ. of Excellence') == 'university of excellence'
    assert normalize('univ of Excellence') == 'university of excellence'
    assert normalize('Excellence Univ') == 'excellence university'
    assert normalize('U. of Excellence') == 'university of excellence'
    assert normalize('U.W.X. of Excellence') == 'u.w.x. of excellence'
    assert normalize('U. W. X. of Excellence') == 'u. w. x. of excellence'
