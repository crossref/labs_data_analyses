class MatchedOrganization:

    def __init__(self, text=None, score=0, matching_type=None,
                 organization=None):
        self.text = text
        self.score = score
        self.matching_type = matching_type
        self.organization = organization
