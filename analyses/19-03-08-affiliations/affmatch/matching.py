import re

from affmatch.config import MIN_MATCHING_SCORE
from affmatch.countries import get_countries
from affmatch.es_utils import es_search, get_query_phrase, \
    get_query_common, get_query_fuzzy, get_query_acronym
from affmatch.organization import MatchedOrganization
from affmatch.scoring import get_score
from affmatch.utils import normalize


MATCHING_TYPE_PHRASE = 'PHRASE'
MATCHING_TYPE_COMMON = 'COMMON TERMS'
MATCHING_TYPE_FUZZY = 'FUZZY'
MATCHING_TYPE_HEURISTICS = 'HEURISTICS'
MATCHING_TYPE_ACRONYM = 'ACRONYM'


def match_by_query(text, matching_type, query, countries):
    candidates = es_search(query)
    if not candidates:
        return MatchedOrganization(text=text, matching_type=matching_type)
    scores = [(candidate, get_score(candidate, text, countries))
              for candidate in candidates]
    max_score = max([s[1] for s in scores])
    best = [s for s in scores if s[1] == max_score][0]
    return MatchedOrganization(text=text, matching_type=matching_type,
                               score=best[1], organization=best[0])


def match_by_type(text, matching_type, countries):
    queries = []
    if matching_type == MATCHING_TYPE_PHRASE:
        queries = [(text, get_query_phrase(normalize(text)))]
    elif matching_type == MATCHING_TYPE_COMMON:
        queries = [(text, get_query_common(normalize(text)))]
    elif matching_type == MATCHING_TYPE_FUZZY:
        queries = [(text, get_query_fuzzy(normalize(text)))]
    elif matching_type == MATCHING_TYPE_ACRONYM:
        queries = [(ac, get_query_acronym(ac))
                   for ac in re.findall('[A-Z]{3,}', text)]
    elif matching_type == MATCHING_TYPE_HEURISTICS:
        h1 = re.search('University of ([^\s]+)', text)
        if h1 is not None:
            queries.append((h1.group(),
                            get_query_common(normalize(h1.group()))))
            queries.append((h1.group(1) + ' University',
                            get_query_common(normalize(h1.group(1) +
                                                       ' University'))))
        h2 = re.search('([^\s]+) University', text)
        if h2 is not None:
            queries.append((h2.group(),
                            get_query_common(normalize(h2.group()))))
            queries.append(('University of ' + h2.group(1),
                            get_query_common(normalize('University of ' +
                                             h2.group(1)))))
    matched = [match_by_query(t, matching_type, q, countries)
               for t, q in queries]
    matched.append(MatchedOrganization(text=text,
                                       matching_type=matching_type))
    max_score = max([m.score for m in matched])
    return [m for m in matched if m.score == max_score][0]


class MatchingNode:

    def __init__(self, text, children=[]):
        self.text = text
        self.children = children
        self.matched = None

    def get_children_max_score(self):
        score = 0
        for child in self.children:
            if child.matched is not None and child.matched.score > score:
                score = child.matched.score
            child_tree_score = child.get_children_max_score()
            if child_tree_score > score:
                score = child_tree_score
        return score

    def remove_descendants_links(self):
        for child in self.children:
            child.matched = None
            child.remove_descendants_links()

    def prune_links(self):
        if self.matched is None:
            return
        children_max_score = self.get_children_max_score()
        if children_max_score >= self.matched.score:
            self.matched = None
        else:
            self.remove_descendants_links()

    def get_matching_types(self):
        if self.children:
            return [MATCHING_TYPE_PHRASE, MATCHING_TYPE_COMMON,
                    MATCHING_TYPE_FUZZY]
        else:
            return [MATCHING_TYPE_PHRASE, MATCHING_TYPE_COMMON,
                    MATCHING_TYPE_FUZZY, MATCHING_TYPE_HEURISTICS]

    def match(self, countries, min_score):
        for matching_type in self.get_matching_types():
            m = match_by_type(self.text, matching_type, countries)
            if self.matched is None:
                self.matched = m
            if self.matched is not None and m.score > self.matched.score:
                self.matched = m
            if self.matched is not None and self.matched.score >= min_score:
                break


class MatchingGraph:

    def __init__(self, affiliation):
        self.nodes = []
        self.affiliation = affiliation
        affiliation = re.sub('&amp;', '&', affiliation)
        prev = []
        for part in [s.strip() for s in re.split('[,;:]', affiliation)]:
            part_norm = re.sub(' and ', ' & ', part)
            if '&' in part_norm:
                part1 = re.sub('&.*', '', part_norm).strip()
                part2 = re.sub('.*&', '', part_norm).strip()
                n1 = MatchingNode(part1)
                n2 = MatchingNode(part2)
                nn = MatchingNode(part, [n1, n2])
                self.nodes.append(n1)
                self.nodes.append(n2)
                self.nodes.append(nn)
                for p in prev:
                    self.nodes.append(MatchingNode(p.text + ' ' + part1,
                                                   [p, n1]))
                    self.nodes.append(MatchingNode(p.text + ' ' + part,
                                                   [p, nn]))
                prev = [n2, nn]
            else:
                n = MatchingNode(part)
                self.nodes.append(n)
                for p in prev:
                    self.nodes.append(MatchingNode(p.text + ' ' + part,
                                                   [p, n]))
                prev = [n]

    def remove_low_scores(self, min_score):
        for node in self.nodes:
            if node.matched is not None and node.matched.score < min_score:
                node.matched = None

    def prune_links(self):
        for node in self.nodes:
            node.prune_links()

    def match(self, countries, min_score):
        for node in self.nodes:
            node.match(countries, min_score)
        self.remove_low_scores(min_score)
        self.prune_links()
        all_matched = []
        for node in self.nodes:
            if node.matched is not None and \
                node.matched.organization['id'] not in [m.organization['id']
                                                        for m in all_matched]:
                all_matched.append(node.matched)
        if not all_matched:
            matched = match_by_type(self.affiliation, MATCHING_TYPE_ACRONYM,
                                    countries)
            if matched.score >= min_score:
                all_matched.append(matched)
        return all_matched


def match_affiliation(affiliation):
    countries = get_countries(affiliation)
    graph = MatchingGraph(affiliation)
    return graph.match(countries, MIN_MATCHING_SCORE)
