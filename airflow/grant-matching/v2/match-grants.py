import json
import re
import sys

from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from pyspark.sql import SparkSession


# Spark script config
CONFIG = json.loads(sys.argv[1])
FUNDERS_FILE = CONFIG["funders-file"]
SNAPSHOT = CONFIG["snapshot"]
OUTPUT = CONFIG["output"]
OUTPUT_GRANT = CONFIG["output-grants"]


# helper metadata functions


def normalize_funder_id(doi):
    return "10.13039/" + re.sub(".*\/", "", doi).lower() if doi else ""


def grant_funder_ids(grant):
    return set(
        [
            normalize_funder_id(fid["id"]).lower()
            for project in grant.get("project", [])
            for funding in project["funding"]
            for fid in funding["funder"]["id"]
        ]
    )


def get_grant_funding(grant):
    awards = []
    funders = grant_funder_ids(grant)
    if "DOI" in grant:
        for f in funders:
            awards.append((grant["DOI"].lower(), grant["DOI"].lower(), f))
    if "DOI" in grant and "award" in grant:
        for f in funders:
            awards.append((grant["DOI"].lower(), grant["award"].lower(), f))
    return awards


def get_awards(funding_object):
    awards = set()
    for award in funding_object.get("award", []):
        awards.add((award, award.lower()))
        for sub_awards in re.findall("10\..*", award):
            awards.add((award, sub_awards.lower()))
        for sub_awards in re.split("[ ,;.|]+", award):
            awards.add((award, sub_awards.lower()))
    return awards


def get_research_output_funding(work):
    awards = []
    for funding_object in work.get("funder", []):
        if "DOI" in funding_object and "DOI" in work:
            for original_award, award in get_awards(funding_object):
                awards.append(
                    (
                        work["DOI"].lower(),
                        work["created"]["date-time"],
                        original_award,
                        award,
                        funding_object.get("DOI", "").lower(),
                    )
                )
    return awards


def match_type(grant_doi, original_award, award):
    if grant_doi.lower() == original_award.lower():
        return "GRANT_DOI"
    if original_award.lower().endswith(grant_doi.lower()):
        return "GRANT_DOI_PARTIAL"
    if original_award.lower() == award.lower():
        return "AWARD"
    if award.lower() in re.split("[ ,;.|]+", original_award.lower()):
        return "AWARD_PARTIAL"
    return ""


def funder_verification_type(funder1, funder2, funders):
    if funder1 == funder2:
        return "FUNDERS_MATCH"

    if funder1 in funders and funder2 in funders:
        funder1_aliases = funders[funder1].get("aliases", [])
        funder2_aliases = funders[funder2].get("aliases", [])
        if funder1 in funder2_aliases or funder2 in funder1_aliases:
            return "FUNDER_ALIAS"

        funder1_family = funders[funder1].get("family", [])
        funder2_family = funders[funder2].get("family", [])
        if funder1 in funder2_family or funder2 in funder1_family:
            return "FUNDER_HIERARCHY"

    return ""


def get_relationships(work):
    rels = []
    if "DOI" not in work:
        return rels

    for r in work.get("relation", {}).get("finances", []):
        if "doi" == r.get("id-type", ""):
            rels.append((work["DOI"].lower(), r.get("id", "").lower(), "REL_FINANCES"))
    for r in work.get("relation", {}).get("is-financed-by", []):
        if "doi" == r.get("id-type", ""):
            rels.append(
                (r.get("id", "").lower(), work["DOI"].lower(), "REL_IS_FINANCED_BY")
            )

    return rels


# Spark session initialization
spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()


# MATCHING BY AWARD NUMBER AND FUNDER

# funders data (aliases and hierarchy information)
funders = spark.sparkContext.wholeTextFiles(FUNDERS_FILE)
funders = json.loads(funders.collect()[0][1])

# user-defined functions
match_type_udf = udf(match_type, StringType())
funder_verification_type_udf = udf(
    lambda x, y: funder_verification_type(x, y, funders), StringType()
)

# main data (snapshot)
data = spark.sparkContext.textFile(SNAPSHOT, minPartitions=10000)
data = data.map(lambda line: json.loads(line))

# get funding information from grants
grants = data.filter(lambda x: x.get("type", "") == "grant")
grants_json = grants.map(json.dumps)
grants_json.saveAsTextFile(OUTPUT_GRANT)

grants = grants.flatMap(get_grant_funding)
grants = spark.createDataFrame(
    grants,
    "grant_doi: string, award: string, funder: string",
)

# get funding information from research outputs
outputs = data.flatMap(get_research_output_funding)
outputs = spark.createDataFrame(
    outputs,
    "output_doi: string, output_created: string, original_award: string, award: string, funder: string",
)

# matching through awards
matches = grants.join(outputs, "award", "inner")
matches = matches.select(
    grants.grant_doi,
    outputs.output_doi,
    outputs.output_created,
    outputs.original_award,
    grants.award.alias("matched_award"),
    grants.funder.alias("grant_funder"),
    outputs.funder.alias("output_funder"),
)

# verification by funder IDs
matches = matches.withColumn(
    "match_type",
    match_type_udf(matches.grant_doi, matches.original_award, matches.matched_award),
)
matches = matches.withColumn(
    "funder_match",
    funder_verification_type_udf(matches.grant_funder, matches.output_funder),
)
matches = matches.filter(
    (matches.funder_match != "")
    | (matches.match_type == "GRANT_DOI")
    | (matches.match_type == "GRANT_DOI_PARTIAL")
)


# GENERIC RELATIONSHIPS "FINANCES" AND "IS-FINANCED-BY"

# all grant DOIs
grant_dois = data.filter(lambda x: x.get("type", "") == "grant")
grant_dois = grant_dois.map(lambda x: (x.get("DOI", "").lower(),))
grant_dois = spark.createDataFrame(
    grant_dois,
    "grant_doi: string",
)

# all research output DOIs
ro_dois = data.filter(lambda x: x.get("type", "") != "grant")
ro_dois = ro_dois.map(lambda x: (x.get("DOI", "").lower(), x["created"]["date-time"]))
ro_dois = spark.createDataFrame(
    ro_dois,
    "output_doi: string, output_created: string",
)

# generic relationships
grant_rels = data.flatMap(get_relationships)
grant_rels = grant_rels.map(lambda x: (x[0], x[1], "", "", "", "", x[2], ""))
grant_rels = spark.createDataFrame(
    grant_rels,
    "grant_doi: string, output_doi: string, original_award: string, matched_award: string, grant_funder: string, output_funder: string, match_type: string, funder_match: string",
)

# joining to filter only existing DOIs
grant_rels = grant_rels.join(grant_dois, "grant_doi", "inner")
grant_rels = grant_rels.select(
    grant_rels.grant_doi,
    grant_rels.output_doi,
    grant_rels.original_award,
    grant_rels.matched_award,
    grant_rels.grant_funder,
    grant_rels.output_funder,
    grant_rels.match_type,
    grant_rels.funder_match,
)

grant_rels = grant_rels.join(ro_dois, "output_doi", "inner")
grant_rels = grant_rels.select(
    grant_rels.grant_doi,
    grant_rels.output_doi,
    ro_dois.output_created,
    grant_rels.original_award,
    grant_rels.matched_award,
    grant_rels.grant_funder,
    grant_rels.output_funder,
    grant_rels.match_type,
    grant_rels.funder_match,
)

# final dataset
matches = matches.union(grant_rels)
matches = matches.distinct()

matches.write.format("json").save(OUTPUT)
