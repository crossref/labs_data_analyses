#!/bin/bash -xe

sudo pip3 install --upgrade pip
sudo /usr/local/bin/pip3 install urllib3==1.26.7
sudo /usr/local/bin/pip3 install requests
sudo /usr/local/bin/pip3 install retry
