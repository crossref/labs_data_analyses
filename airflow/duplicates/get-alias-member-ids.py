import json
import requests
import sys
import time

from pyspark.sql import SparkSession
from retry import retry
from xml.etree import ElementTree


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG['input']
OUTPUT = CONFIG['output']
API_URL = CONFIG['api-url']
MAILTO = CONFIG['mailto']


@retry(tries=5, delay=10)
def get_member_id(doi):
    time.sleep(1)
    content = requests.get(API_URL, {'pid': MAILTO, 'format': 'unixsd', 'id': doi}).content
    root = ElementTree.fromstring(content)
    
    member_ids = root.findall('.//c:crm-item[@name="member-id"]',
                              namespaces={'c': 'http://www.crossref.org/qrschema/3.0'})
    member_id = None
    if member_ids:
        member_id = member_ids[0].text

    return member_id

spark = SparkSession.builder \
            .config('spark.jars.packages', 'org.apache.hadoop:hadoop-aws:2.8.5') \
            .getOrCreate()

aliases = spark.sparkContext.textFile(INPUT, minPartitions=1000)
aliases = aliases.map(lambda r: r.split(','))
aliases = aliases.map(lambda r: (r[0], r[1], get_member_id(r[0]), get_member_id(r[1])))
aliases = spark.createDataFrame(aliases)
aliases.write.format('csv').save(OUTPUT)
