import argparse
import base64
import boto3
import json


def get_secret(secret_name):
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=region_name)
    response = client.get_secret_value(SecretId=secret_name)
    return response.get(
        "SecretString", base64.b64decode(response.get("SecretBinary", ""))
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process affiliations a snapshot.")
    parser.add_argument("-i", "--input", type=str, required=True)
    parser.add_argument("-o", "--output", type=str, required=True)
    parser.add_argument("-a", "--affiliations", type=str, required=False)
    parser.add_argument("-s", "--script", type=str, required=True)
    args = parser.parse_args()

    client = boto3.client("emr", region_name="us-east-1")

    response = client.run_job_flow(
        Name="affiliation-matching",
        ReleaseLabel="emr-6.15.0",
        Applications=[{"Name": "Spark"}],
        Instances={
            "MasterInstanceType": "m5.xlarge",
            "SlaveInstanceType": "m5.xlarge",
            "InstanceCount": 17,
            "KeepJobFlowAliveWhenNoSteps": False,
            "TerminationProtected": False,
        },
        Steps=[
            {
                "Name": "affiliation-matching",
                "ActionOnFailure": "TERMINATE_CLUSTER",
                "HadoopJarStep": {
                    "Jar": "command-runner.jar",
                    "Args": [
                        "spark-submit",
                        "--deploy-mode",
                        "cluster",
                        f"s3://{get_secret('bucket-name-emr-code')}/affiliation-matching/{args.script}",
                        json.dumps(
                            {
                                "input": args.input,
                                "output": args.output,
                                "affiliations": args.affiliations,
                            }
                        ),
                    ],
                },
            }
        ],
        LogUri=get_secret("s3-url-logs"),
        VisibleToAllUsers=True,
        ServiceRole="EMR_DefaultRole",
        JobFlowRole="EMR_EC2_DefaultRole",
    )

    print(json.dumps(response, indent=4, sort_keys=True, default=str))
