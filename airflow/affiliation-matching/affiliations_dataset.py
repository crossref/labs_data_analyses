import json
import sys

import pyspark.sql.functions as F
from pyspark.sql.functions import udf
from pyspark.sql import SparkSession


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
AFFS = CONFIG["affiliations"]
OUTPUT = CONFIG["output"]


def get_affiliation(item):
    ror_id = [i["id"] for i in item.get("id", []) if "ROR" == i.get("id-type", "")]
    ror_id = ror_id[0] if ror_id else None
    return item.get("name", None), ror_id, "publisher" if ror_id is not None else None


def get_contributor(data):
    contributor = {}
    for k in data:
        if k not in ["sequence", "affiliation", "role-start", "role-end"]:
            contributor[k] = data[k]
    return json.dumps(contributor)


def get_affiliations(work):
    doi = work.get("DOI", "")
    if not doi:
        return []

    affiliations = []

    for ct in ["author", "editor", "translator", "chair"]:
        for i, c in enumerate(work.get(ct, [])):
            for a in c.get("affiliation", []):
                aff, ror_id, a_by = get_affiliation(a)
                contr = get_contributor(c)
                affiliations.append((doi, ct, i + 1, contr, aff, ror_id, a_by))

    for i, a in enumerate(work.get("institution", [])):
        aff, ror_id, a_by = get_affiliation(a)
        affiliations.append((doi, "institution", i + 1, None, aff, ror_id, a_by))

    for it in ["investigator", "lead-investigator", "co-lead-investigator"]:
        for p in work.get("project", []):
            for i, c in enumerate(p.get(it, [])):
                for a in c.get("affiliation", []):
                    aff, ror_id, a_by = get_affiliation(a)
                    contr = get_contributor(c)
                    affiliations.append((doi, it, i + 1, contr, aff, ror_id, a_by))

    return affiliations


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()


data = spark.sparkContext.textFile(INPUT, minPartitions=10000)
data = data.map(lambda line: json.loads(line))
data = data.flatMap(get_affiliations)
data = spark.createDataFrame(
    data,
    "doi: string, rel: string, seq_no: int, contributor: string, affiliation: string, ror_id: string, ror_id_asserted_by: string",
)

data_publisher = data.filter(
    (data.ror_id_asserted_by == "publisher") | (data.affiliation.isNull())
)

data_crossref = data.filter(
    (data.ror_id_asserted_by.isNull()) & (data.affiliation.isNotNull())
)

affiliations = spark.sparkContext.textFile(AFFS, minPartitions=10000)
affiliations = affiliations.map(lambda line: json.loads(line))
affiliations = affiliations.map(
    lambda x: (x["affiliation"], x["ror-id"] if x["ror-id"] else None)
)
affiliations = spark.createDataFrame(
    affiliations, "affiliation: string, ror_id_matched: string"
)

data_crossref = data_crossref.join(affiliations, "affiliation", "left")
data_crossref = data_crossref.select(
    data_crossref.doi,
    data_crossref.rel,
    data_crossref.seq_no,
    data_crossref.contributor,
    data_crossref.affiliation,
    data_crossref.ror_id_matched.alias("ror_id"),
    data_crossref.ror_id_asserted_by,
)
data_crossref = data_crossref.withColumn(
    "ror_id_asserted_by",
    F.when(data_crossref.ror_id.isNotNull(), "crossref").otherwise(None),
)

data_all = data_publisher.union(data_crossref)


@udf
def to_obj(doi, rel, order, contributor, affiliation, ror_id, ror_id_asserted_by):
    return json.dumps(
        {
            "DOI": doi,
            "rel": rel,
            "order": order,
            "contributor": json.loads(contributor) if contributor is not None else None,
            "affiliation": affiliation,
            "ror_id": ror_id,
            "ror_id_asserted_by": ror_id_asserted_by,
        },
        separators=(",", ":"),
        ensure_ascii=False,
    )


data_all = data_all.select(
    to_obj(
        "doi",
        "rel",
        "seq_no",
        "contributor",
        "affiliation",
        "ror_id",
        "ror_id_asserted_by",
    )
)

data_all.write.format("text").save(OUTPUT)
