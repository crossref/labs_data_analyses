import json
import sys

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode


CONFIG = json.loads(sys.argv[1])
INPUT = CONFIG["input"]
OUTPUT = CONFIG["output"]


spark = SparkSession.builder.config(
    "spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.8.5"
).getOrCreate()


raw_data = spark.sparkContext.textFile(INPUT, minPartitions=10000)
data = raw_data.map(lambda line: json.loads(line))


def cited_dois(work):
    references = work.get("reference", [])
    return [r["DOI"] for r in references if "DOI" in r]


citations = data.map(lambda work: (work["DOI"], cited_dois(work)))
citations = spark.createDataFrame(citations, ["doi", "cited_dois"])
citations = citations.select(citations.doi, explode(citations.cited_dois))
citation_counts = citations.groupBy("col").count()

citation_counts.write.format("csv").save(OUTPUT)
